const override = process.env.REACT_APP_SERVER_ADDRESS_OVERRIDE
  ? new URL(process.env.REACT_APP_SERVER_ADDRESS_OVERRIDE)
  : null;

//TODO make overrides environment variables
const host = override?.host || window.location.host;
const baseUrl = override?.origin || window.location.origin;
const apiUrl = `${baseUrl}/api`;

const wsProtocol = (() => {
  if ((override || window.location).protocol.includes("https")) {
    return "wss";
  } else {
    return "ws";
  }
})();
const wsUrl = `${wsProtocol}://${host}/ws`;

export { baseUrl, apiUrl, wsUrl };
