const wait = (duration: number) =>
  new Promise((res, rej) => setTimeout(res, duration));

export { wait };
