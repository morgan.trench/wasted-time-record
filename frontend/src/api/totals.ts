import Axios from "axios";
import { apiUrl, wsUrl } from "./common";
import { DateTime, Interval } from "luxon";

interface Range {
  from: DateTime;
  to: DateTime;
}
export interface RangeResult {
  interval: Interval;
  total: number;
}
const getTotals: (boundaries: Range[]) => Promise<RangeResult[]> = (
  boundaries: Range[]
) =>
  Axios.post(`${apiUrl}/totals`, {
    boundaries: boundaries.map((x) => ({
      from: x.from.toMillis(),
      to: x.to.toMillis(),
    })),
  }).then((res) =>
    res.data.map((x: any) => ({
      ...x,
      interval: Interval.fromDateTimes(
        DateTime.fromMillis(x.from),
        DateTime.fromMillis(x.to)
      ),
    }))
  );

const setupSocketForUpdates = () => new WebSocket(`${wsUrl}/totals`);

export { getTotals, setupSocketForUpdates };
