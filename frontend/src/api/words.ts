import Axios, { AxiosResponse } from "axios";
import { apiUrl, wsUrl } from "./common";

const getWord: (word: string) => Promise<AxiosResponse<any>> = (word) =>
  Axios.get(`${apiUrl}/words/${word}`);

const getWords: () => Promise<AxiosResponse<any>> = () =>
  Axios.get(`${apiUrl}/words`);

const setupSocketForUpdates = () => new WebSocket(`${wsUrl}/words`);

export { getWord, getWords, setupSocketForUpdates };
