import Axios from "axios";
import { Complaint } from "../model";
import { apiUrl } from "./common";

const submitRecord = (complaint: Complaint) =>
  Axios.post(`${apiUrl}/record`, complaint);

export {
    submitRecord
}
