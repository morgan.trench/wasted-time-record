export type Complaint = {
    wastage: number,
    complaint: string
}
