import { Cloud, Help, Add, Assessment } from "@material-ui/icons";
import React, { useState } from "react";
import { SpeedDial, SpeedDialIcon, SpeedDialAction } from "@material-ui/lab";
import { useHistory, useLocation } from "react-router-dom";
import { useMediaQuery } from 'react-responsive'
import Submitter from "./Submitter/Submitter"
import WordCloud from "./WordCloud/WordCloud"
import Contact from "./Contact/Contact"
import Totals from "./Totals/Totals"

interface Route {
  path: string,
  component: JSX.Element,
  icon: JSX.Element,
  tooltip: string
}

const listing: Route[] = [
  {
    path: "/about",
    component: <Contact />,
    icon: <Help />,
    tooltip: "About",
  },
  {
    path: "/cloud",
    component: <WordCloud />,
    icon: <Cloud />,
    tooltip: "Word Cloud",
  },
  {
    path: "/totals",
    component: <Totals />,
    icon: <Assessment />,
    tooltip: "Totals",
  },
  {
    path: "/",
    component: <Submitter />,
    icon: <Add />,
    tooltip: "Submit"
  },
];


const NavMenu = () => {
  const [openNavDial, setOpenNavDial] = useState(false);
  const isMobile = useMediaQuery({ query: '(max-width: 700px)' })
  const location = useLocation();
  const history = useHistory();

  return <SpeedDial
    className="speed-dial-nav"
    ariaLabel="Navigation Menu"
    icon={listing.find(route => route.path === location.pathname)?.icon || <SpeedDialIcon />}
    onClose={() => setOpenNavDial(false)}
    open={openNavDial}
    onOpen={() => setOpenNavDial(true)}
    direction={isMobile ? "left" : "right"}
  >
    {listing.filter(route => route.path !== location.pathname).reverse().map((route) => ( // reverse is 'in-place', but this is ok as splice is a shallow copy
      <SpeedDialAction
        key={route.path}
        icon={route.icon}
        tooltipTitle={route.tooltip}
        onClick={() => history.push(route.path)}
        tooltipPlacement={isMobile ? "top" : "bottom"}
      />
    ))}
  </SpeedDial>
}

export { listing, NavMenu };
