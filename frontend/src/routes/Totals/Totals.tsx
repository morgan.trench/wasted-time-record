import { Switch } from "@material-ui/core";
import { DateTime } from "luxon";
import React, { useEffect, useState } from "react";
import { Bar, BarChart, Label, Legend, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';
import 'tippy.js/animations/scale.css';
import 'tippy.js/dist/tippy.css';
import { getTotals, RangeResult, setupSocketForUpdates } from '../../api/totals';
import "./Totals.css";

interface TotalUpdateMessage {
  duration: number,
  timestamp: number
}

const MonthlyBreakdownChart = ({ totals }: { totals: RangeResult[] }) => {
  const UnitLabel = ({ unit, value }: { unit: { unit: string, factor: number }, value: number }) => {
    return <div>
      {(value / unit.factor < 1 && unit.unit !== "seconds") ? "" : `${value / unit.factor} ${unit.unit}`}
    </div>
  }

  const CustomTooltip = ({ active, payload, label }: { active: any, payload: any, label: any }) => {
    if (active && payload != null) {
      const units = [{ unit: "seconds", factor: 1 }, { unit: "hours", factor: 3600 }, { unit: "days", factor: 7.6 * 3600 }]
      const value = payload[0].value
      return (
        <div style={{ padding: '5px', backgroundColor: "rgb(255 255 255 / 20%" }}>
          <div>label</div>
          {units.map(unit => (<UnitLabel unit={unit} value={value} />))}
        </div>
      );
    }

    return null;
  };

  return (
    <ResponsiveContainer width="80%" height="80%" minWidth="600px" minHeight="400px">
      <BarChart data={totals} margin={{ top: 25, right: 25, left: 25, bottom: 25 }}>
        <XAxis dataKey="label">
          <Label value="Period" position="bottom" />
        </XAxis>
        <YAxis>
          <Label value="Wasted Seconds" position="insideLeft" angle={-90} />
        </YAxis>
        <Tooltip content={CustomTooltip} />
        <Bar dataKey="total" fill="#8884d8" />
      </BarChart>
    </ResponsiveContainer>
  )
}

const MilestonesPlot = ({ totals }: { totals: RangeResult[] }) => {
  const series = totals.reduce<{ runningTotal: number, list: { timestamp: number, total: number }[] }>((acc, x) => {
    acc.runningTotal += x.total
    acc.list.push({ timestamp: (x.interval.start.toMillis() + x.interval.end.toMillis()) / 2, total: acc.runningTotal })
    return acc
  }, { runningTotal: 0, list: [] }).list

  const TickFormatter = (props: any) => {
    console.log(props)
    const { x, y, payload }: { x: number, y: number, payload: any } = props
    const { value }: { value: number } = payload
    const contents = DateTime.fromMillis(value).toISOWeekDate()
    return (
      <g
        transform={`translate(${x + 20},${y})`}
      >
        <text x={0} y={0} dy={16} textAnchor="end" fill="#666" transform="rotate(-45)">{contents}</text>
      </g>
    );
  }

  return (
    <ResponsiveContainer width="80%" height="80%" minWidth="600px" minHeight="400px">
      <LineChart data={series}
        margin={{ top: 25, right: 25, left: 25, bottom: 75 }}>
        {/* <CartesianGrid strokeDasharray="3 3" /> */}
        <XAxis dataKey="timestamp" tick={<TickFormatter />} />
        <YAxis />
        <Tooltip />
        <Legend align="center" verticalAlign="top" />
        <Line type="linear" dataKey="total" stroke="#8884d8" />
      </LineChart>
    </ResponsiveContainer>
  )
}

const Totals = () => {
  const [monthlyTotals, setMonthlyTotals] = useState<RangeResult[]>([]) // TODO build monthly totals from the daily ones
  const [dailyTotals, setDailyTotals] = useState<RangeResult[]>([])

  const [ws, setWs] = useState<WebSocket | null>()
  const [monthly, setMonthly] = useState<boolean>(false)

  useEffect(() => {
    const now = DateTime.fromMillis(Date.now())

    const dailyBoundaries = [now, ...[...new Array(365)].map((_, i) => (now.minus({ day: i + 1 })))]
      .map(x => ({ from: x.startOf("day"), to: x.endOf("day") }))

    const monthlyBoundaries = [now, ...[...new Array(12)].map((_, i) => (now.minus({ month: i + 1 })))]
      .map(x => ({ from: x.startOf("month"), to: x.endOf("month") }))

    getTotals(dailyBoundaries).then(data => {
      setDailyTotals(data.map((x, i: number) => ({ ...x, label: x.interval.start.monthShort })));
    })

    getTotals(monthlyBoundaries).then(data => {
      setMonthlyTotals(data.map((x, i: number) => ({ ...x, label: x.interval.start.monthShort })));
    })
  }, [])

  useEffect(() => {
    if (monthlyTotals.length > 0 || dailyTotals.length > 0) {
      const ws = setupSocketForUpdates()
      setWs(ws)
      return () => {
        ws.close()
        setWs(null)
      }
    }
  }, [monthlyTotals, dailyTotals])

  useEffect(() => {
    const onMessage = async (ev: MessageEvent) => {
      const message: TotalUpdateMessage = JSON.parse(ev.data)
      const timestamp = DateTime.fromMillis(message.timestamp)

      const updateCollection = (collection: RangeResult[], setCollection: React.Dispatch<React.SetStateAction<RangeResult[]>>) => {
        const totalClone = [...collection]
        const element = totalClone.find(x => x.interval.contains(timestamp))
        if (element != null) {
          element.total += message.duration
          setCollection(totalClone)
        }
      }

      updateCollection(monthlyTotals, setMonthlyTotals)
      updateCollection(dailyTotals, setDailyTotals)

    }
    ws?.addEventListener('message', onMessage)
    return () => {
      ws?.removeEventListener('message', onMessage)
    }
  }, [ws, monthlyTotals, dailyTotals])

  return (
    <div style={{ height: '100vh', width: '100vw' }}>
      <div style={{ height: '100%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
        {monthly && <MonthlyBreakdownChart totals={monthlyTotals} />}
        {!monthly && <MilestonesPlot totals={dailyTotals} />}
        <div>
          All Time
          <Switch
            color="primary"
            checked={monthly}
            onChange={() => setMonthly(!monthly)}
          />
          Monthly Breakdown
        </div>
      </div>
    </div>
  );
};

export default Totals;
