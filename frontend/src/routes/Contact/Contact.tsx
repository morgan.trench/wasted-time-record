import {
  Box,
  Card,
  CardContent,
  Container,
  Typography,
} from "@material-ui/core";
import React from "react";
import "./Contact.css";

const Contact = () => {
  return (
    <>
      <Container maxWidth="md">
        <Card>
          <CardContent>
            <Box>
              <Typography variant="h5"> About </Typography>
              This website is intended to be a place where people can record
              work-related frustrations to do with wasted time.<br></br>The idea is that eventually we might be able to point at the 'total time wasted on X' and get something done about it.
            </Box>
            {/* <Box style={{ marginTop: "15px" }}>
              <Typography variant="h5"> Work In Progress </Typography>
              This a project currently being developed in my spare time as a learning exercise to maintain an 'actual' web application myself as well as experiment with
              some stuff including:<br></br>
              <ul style={{ display: 'inline-block' }}>
                <li style={{ textAlign: 'left' }}>React</li>
                <li style={{ textAlign: 'left' }}>Ktor</li>
                <li style={{ textAlign: 'left' }}>Kodein</li>
                <li style={{ textAlign: 'left' }}>Gitlab CI/CD with the AWS CDK</li>
              </ul>
            </Box> */}
            <Box style={{ marginTop: "15px" }}>
              <Typography variant="h5"> Contact </Typography>
              If you experience any problems or have any suggestions, please contact me at <i>morgan.trench@gmail.com</i>
            </Box>
          </CardContent>
        </Card>
      </Container>
    </>
  );
};

export default Contact;
