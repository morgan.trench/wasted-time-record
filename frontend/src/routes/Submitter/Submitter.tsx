import {
  Button,
  Card,
  CardActions,
  CardContent,
  CircularProgress,
  Snackbar,
  TextField,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Container } from "@material-ui/core";
import MuiAlert from '@material-ui/lab/Alert';
import { duration, unitOfTime, Duration } from "moment";
import React, { useState, useEffect } from "react";
import { ChangeEventHandler } from "react";
import { submitRecord } from "../../api/records";
import { wait } from "../../api/util"
import "./Submitter.css";

const units: unitOfTime.Base[] = ["hours", "minutes", "seconds"];
const baseUnit: unitOfTime.Base = "milliseconds";
const getBaseUnit: keyof { [key in keyof Duration]: () => number } = "asMilliseconds";
const maxUnit: unitOfTime.Base = "hours";


const numberInputStyles = makeStyles({
  root: {
    "& label": {
      top: "auto",
      bottom: "0px",
    },
    "& div": {
      marginTop: "0px",
      marginBottom: "16px",
      fontSize: "inherit",
    }
  },
});

const TimeInput = ({
  value,
  set,
  applyDiff,
  unit,
}: {
  value: number;
  set: (newValue: number) => void;
  applyDiff: (newValDifference: number) => void;
  unit: string;
}) => {
  const [focussed, setFocussed] = useState(false);
  const classes = numberInputStyles();
  const onChangeNumber = (event: any, child?: any) => {
    set(parseInt(event.target.value));
  };
  const Wheely = (event: any) => {
    event.stopPropagation();
    if (!focussed) {
      applyDiff(event.deltaY < 0 ? 1 : -1);
    }
  };

  return (
    <TextField
      className={classes.root}
      value={value}
      onChange={onChangeNumber}
      onFocus={() => setFocussed(true)}
      onBlur={() => setFocussed(false)}
      onWheel={Wheely}
      type="number"
      inputProps={{ style: { textAlign: "center" } }}
      style={{
        margin: "0em 0.5em",
        width: "min-content",
        minWidth: `${Math.max(
          Math.floor(Math.log10(value) + 2) * 0.55,
          1.5
        )}em`,
      }}
      label={unit}
    />
  );
};

const useTime = (tickInterval: number) => {
  const [time, setTime] = useState<number>(Date.now());
  useEffect(() => {
    const timeoutRef = setInterval(() => setTime(Date.now()), tickInterval);
    return () => clearInterval(timeoutRef);
  }, [tickInterval, setTime]);
  return time;
};

const Submitter = () => {
  const time = useTime(1000);
  const [origin, setOrigin] = useState<number>(Date.now());
  const [complaint, setComplaint] = useState<string>("");

  const [sending, setSending] = useState(false);
  const [showSnackbar, setShowSnackbar] = useState(false);

  const wastage = time - origin;

  const setValueForUnit = (unit: unitOfTime.Base) => (amount: number) => {
    const old = duration(wastage, baseUnit);
    setOrigin(
      time -
      Math.max(
        old
          .subtract(old[unit !== maxUnit ? "get" : "as"](unit), unit)
          .add(amount, unit)
        [getBaseUnit](),
        0
      )
    );
  };
  const applyDifferenceToUnit = (unit: unitOfTime.Base) => (amount: number) => {
    setOrigin(
      time -
      Math.max(
        duration(wastage, baseUnit).add(amount, unit)[getBaseUnit](),
        0
      )
    );
  };

  const Wheely = (event: any) => {
    event.stopPropagation();
    applyDifferenceToUnit("seconds")(event.deltaY < 0 ? 1 : -1);
  };

  const onComplaintTextFieldChange: ChangeEventHandler<HTMLTextAreaElement> = (
    event: any
  ) => {
    setComplaint(event.target.value);
  };

  const reset = () => {
    setOrigin(Date.now());
    setComplaint("");
  };

  const submit = () => {
    setSending(true);
    const submission = submitRecord({
      wastage: Math.ceil(wastage / 1000),
      complaint,
    })
    Promise.all([submission, wait(500)])
      .finally(() => {
        reset()
        setSending(false);
        setShowSnackbar(true)
      });
  };

  return (
    <>
      <Container maxWidth="sm">
        <Card onWheel={Wheely} style={{ position: "relative" }}>
          <CardContent>
            <div className="sending" style={{ display: sending ? undefined : 'none' }}>
              <CircularProgress />
            </div>
            <div className="submission-blur-content" style={{ filter: sending ? 'blur(6px)' : 'blur(0px)' }}>
              <Typography color="textSecondary" gutterBottom>
                I just wasted
              </Typography>
              <div style={{ fontSize: "36px" }}>
                {units
                  .map((unit, index) => (
                    <TimeInput
                      key={unit}
                      unit={unit}
                      set={setValueForUnit(unit)}
                      applyDiff={applyDifferenceToUnit(unit)}
                      value={
                        unit !== maxUnit
                          ? duration(wastage).get(unit)
                          : Math.floor(duration(wastage).as(unit))
                      }
                    />
                  ))
                  .flatMap((val, index) =>
                    index === units.length - 1
                      ? [val]
                      : [val, <span key={index}>:</span>]
                  )}
              </div>
              <Typography color="textSecondary" gutterBottom>
                because
              </Typography>
              <TextField
                value={complaint}
                onChange={onComplaintTextFieldChange}
                onWheel={(event) => event.stopPropagation()}
                style={{ width: "90%" }}
                multiline
                rows={4}
                variant="outlined"
              />
            </div>

          </CardContent>
          <CardActions style={{ justifyContent: "flex-end" }}>
            <Button size="small" className="submission-blur-content" style={{ filter: sending ? 'blur(6px)' : 'blur(0px)' }} onClick={submit}>
              Submit
              </Button>
          </CardActions>
        </Card>
      </Container>
      <Snackbar open={showSnackbar} autoHideDuration={3000} onClose={() => setShowSnackbar(false)}>
        <MuiAlert variant="filled" severity="success">Success</MuiAlert>
      </Snackbar>
    </>
  );
};

export default Submitter;
