import React, { Dispatch, useEffect, useState } from "react";
import ReactWordcloud from 'react-wordcloud';
import { getWord, getWords, setupSocketForUpdates } from '../../api/words'
import { zipObject } from 'lodash'
import 'tippy.js/animations/scale.css';
import 'tippy.js/dist/tippy.css';
import "./WordCloud.css";


interface WordUsage {
  text: string;
  value: number;
}
const WordCloud = () => {
  const [words, setWords] = useState<Array<WordUsage>>([])
  const [ws, setWs] = useState<WebSocket>()
  const [spiral, setSpiral]: ['archimedean' | 'rectangular', Dispatch<any>] = useState('archimedean')

  useEffect(() => {
    const flipSpiral = setTimeout(() => setSpiral(spiral === 'archimedean' ? 'rectangular' : 'archimedean'), 60 * 1000)
    return () => clearTimeout(flipSpiral)
  }, [spiral])

  useEffect(() => {
    getWords().then(res => {
      setWords(Object.entries(res.data).map((entry: any) => ({ text: entry[0], value: entry[1] })));
    })
  }, [])

  useEffect(() => {
    const ws = setupSocketForUpdates()
    setWs(ws)
    return () => {
      ws.close()
      setWs(undefined)
    }
  }, [])

  useEffect(() => {
    const onMessage = async (ev: MessageEvent) => {
      const message = JSON.parse(ev.data)
      if (message.type === "Recorded") {
        const wordsToIncrement = message.words
        const newWords = [...words]
        wordsToIncrement.forEach((word: string) => {
          const existing = newWords.find(wordUsage => wordUsage.text === word)
          if (existing) {
            existing.value = existing.value + 1
          } else {
            newWords.push({ text: word, value: 1 })
          }
        });
        setWords(newWords)
      } else if (message.type === "FilterAdded") {
        const wordsToRemove: Array<string> = message.words
        setWords([...words].filter((word) => !wordsToRemove.includes(word.text)))
      } else if (message.type === "FilterRemoved") {
        const wordsToAdd: Array<string> = message.words
        const values = await Promise.all(wordsToAdd.map(word => getWord(word))).then(responses => responses.map(response => response.data))
        if (wordsToAdd.length === values.length) {
          const toAppend = Object.entries(zipObject(wordsToAdd, values)).map(([text, value]) => ({ text, value }))
          const newWords = [...words, ...toAppend].sort((a, b) => b.value - a.value);
          setWords(newWords)
        } else {
          // TODO process desync
        }
      }
    }
    ws?.addEventListener('message', onMessage)
    return () => {
      ws?.removeEventListener('message', onMessage)
    }
  }, [ws, words])

  return (
    <div style={{ height: '100vh', width: '100vw' }}>
      <ReactWordcloud
        words={words}
        maxWords={200}
        options={{
          deterministic: false,
          rotations: 3,
          rotationAngles: [-45, 45],
          transitionDuration: 1000,
          fontFamily: "impact",
          fontStyle: 'normal',
          fontWeight: 'normal',
          fontSizes: [30, 100],
          scale: "log",
          spiral,
          padding: 1
        }}
      />
    </div>
  );
};

export default WordCloud;
