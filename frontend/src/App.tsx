import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { listing, NavMenu } from "./routes/routes"
import "./App.css";

const App = () => {
  return (
    <div className="App">
      <Router>
        <Switch>
          {listing.map(route => [
            <Route key={route.path} path={route.path}>
              {route.component}
              <NavMenu key={route.path + ' nav'} />
            </Route>
          ])}
        </Switch>
      </Router>
    </div>
  )
}

export default App;
