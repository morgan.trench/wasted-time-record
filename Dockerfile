FROM gradle:6.7 as ktor_build
WORKDIR /home/gradle
COPY server ./server
WORKDIR /home/gradle/server
RUN gradle distTar --no-daemon


FROM node:latest as spa_build
WORKDIR /home/nodejs
COPY frontend ./frontend
WORKDIR /home/nodejs/frontend
RUN npm ci && npm run build
RUN tar -zcvf build.tar.gz build
RUN echo -n "export SPA_ROUTES=" > spa_variables.sh
RUN cat src/routes/routes.tsx | grep 'path: \"' | awk -F '"' '{print $2}' | sed 's/^\///' | sed '/^$/d' | paste -d, -s >> spa_variables.sh
RUN cat spa_variables.sh


FROM openjdk:latest
WORKDIR /usr/wastage
# Copy and extract server, move its contents to the 'dist' directory, cleanup unnessary files
COPY --from=ktor_build /home/gradle/server/build/distributions/wasted-server-0.0.1.tar .
RUN tar -xvf wasted-server-0.0.1.tar && mkdir dist && mv wasted-server-0.0.1/* dist && rm -rf wasted-server-0.0.1.tar && rm -rf wasted-server-0.0.1 

# Copy and extract frontend, move its contents to the 'dist/public' directory, cleanup unnessary files
COPY --from=spa_build /home/nodejs/frontend/build.tar.gz .
RUN tar -zxvf build.tar.gz && mkdir dist/public && mv build/* dist/public && rm -rf build.tar.gz && rm -rf build

# Copy script that contains the calculated SPA_ROUTES environment variable, create a 'new' startup script that sources those values
COPY --from=spa_build /home/nodejs/frontend/spa_variables.sh .

EXPOSE 8080
CMD ["sh", "-c", ". ./spa_variables.sh; cd dist; ./bin/wasted-server"]