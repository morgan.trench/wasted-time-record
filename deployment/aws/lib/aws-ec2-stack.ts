import {
  CfnEIPAssociation,
  Instance,
  InstanceType,
  MachineImage,
  SubnetType,
  UserData,
  SecurityGroup,
  Peer,
  Port,
  Vpc,
} from "@aws-cdk/aws-ec2";
import {
  HostedZone,
  IHostedZone,
  ARecord,
  RecordTarget,
} from "@aws-cdk/aws-route53";
import { CfnOutput, Construct, Stack, StackProps } from "@aws-cdk/core";

export class AwsEc2Stack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const hostedZone = HostedZone.fromLookup(this, "Z0022604B00UPOO7I57T", {
      domainName: "throwaway.ninja",
      privateZone: false,
    });

    const vpc = new Vpc(this, "WastageVPC", {
      natGateways: 0,
      subnetConfiguration: [
        {
          name: "WastageVpcPublicSubnet Subnet",
          subnetType: SubnetType.PUBLIC,
        },
      ],
    });

    const securityGroup = new SecurityGroup(this, "WastageSecurityGroup", {
      vpc,
    });
    securityGroup.addIngressRule(
      Peer.anyIpv4(),
      Port.tcp(22),
      "SSH from anywhere"
    );
    securityGroup.addIngressRule(Peer.anyIpv4(), Port.tcp(80), "HTTP");
    securityGroup.addIngressRule(Peer.anyIpv4(), Port.tcp(443), "HTTPS");

    const ubuntuImage = MachineImage.lookup({
      name: "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20200408",
    });

    const userData = UserData.forLinux();

    const ec2Instance = new Instance(this, "WastageInstance", {
      vpc,
      instanceType: new InstanceType("t3a.micro"),
      machineImage: ubuntuImage,
      keyName: "WastageDeployment",
      securityGroup: securityGroup,
      vpcSubnets: { subnetType: SubnetType.PUBLIC },
      userData,
    });

    new CfnOutput(this, "PublicIp", {
      value: ec2Instance.instancePublicIp,
    });

    new ARecord(this, "WastageARecord", {
      recordName: "wastage",
      zone: hostedZone,
      target: RecordTarget.fromIpAddresses(ec2Instance.instancePublicIp),
    });

    new CfnOutput(this, "WastageInstancePublicIp", {
      value: ec2Instance.instancePublicIp,
    });
  }
}
