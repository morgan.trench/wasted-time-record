import { DnsValidatedCertificate } from "@aws-cdk/aws-certificatemanager";
import { InstanceType, SubnetType, Vpc, Port } from "@aws-cdk/aws-ec2";
import {
  Cluster,
  ContainerImage,
  Ec2Service,
  Ec2TaskDefinition,
  MachineImageType,
  Secret,
} from "@aws-cdk/aws-ecs";
import {
  ApplicationLoadBalancer,
  ApplicationProtocol,
} from "@aws-cdk/aws-elasticloadbalancingv2";
import {
  ARecord,
  HostedZone,
  IHostedZone,
  RecordTarget,
} from "@aws-cdk/aws-route53";
import {
  Construct,
  Duration,
  Stack,
  StackProps,
  NestedStack,
} from "@aws-cdk/core";
import { LoadBalancerTarget } from "@aws-cdk/aws-route53-targets";
import {
  DatabaseInstance,
  DatabaseInstanceEngine,
  PostgresEngineVersion,
} from "@aws-cdk/aws-rds";

export class WastageEcsStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const infrastuctureStack = new WastageEcsInfrastureStack(
      this,
      "Infrastructure"
    );

    const statefulStack = new WastageEcsStatefulStack(
      this,
      "Stateful",
      infrastuctureStack
    );
    statefulStack.addDependency(infrastuctureStack);

    const ephemeralStack = new WastageEcsEphemeralStack(
      this,
      "Ephemeral",
      statefulStack,
      infrastuctureStack
    );
    ephemeralStack.addDependency(statefulStack);
    ephemeralStack.addDependency(infrastuctureStack);

    const routingStack = new WastageEcsRoutingStack(
      this,
      "Routing",
      infrastuctureStack
    );
    routingStack.addDependency(infrastuctureStack);
  }
}

class WastageEcsInfrastureStack extends NestedStack {
  vpc: Vpc;
  hostedZone: IHostedZone;
  cluster: Cluster;
  certificate: DnsValidatedCertificate;
  loadBalancer: ApplicationLoadBalancer;

  constructor(scope: Construct, id: string) {
    super(scope, id);

    this.vpc = new Vpc(this, "WastageEcsVPC", {
      natGateways: 1,
      subnetConfiguration: [
        {
          name: "WastageEcsVpcPublicSubnet",
          subnetType: SubnetType.PUBLIC,
        },
        {
          name: "WastageEcsVpcPrivateSubnet",
          subnetType: SubnetType.PRIVATE,
        },
      ],
    });

    this.hostedZone = HostedZone.fromLookup(this, "Z0022604B00UPOO7I57T", {
      domainName: "throwaway.ninja",
      privateZone: false,
    });

    this.cluster = new Cluster(this, "Cluster", {
      vpc: this.vpc,
    });

    this.cluster.addCapacity("WastageEc2", {
      minCapacity: 1,
      maxCapacity: 2,
      instanceType: new InstanceType("t3a.micro"),
      machineImageType: MachineImageType.BOTTLEROCKET,
    });

    this.certificate = new DnsValidatedCertificate(this, "WastageCertificate", {
      hostedZone: this.hostedZone,
      domainName: "wastage.throwaway.ninja",
    });

    this.loadBalancer = new ApplicationLoadBalancer(
      this,
      "WastageServerEc2ApplicationLoadBalancer",
      {
        vpc: this.vpc,
        internetFacing: true,
      }
    );
  }
}

class WastageEcsStatefulStack extends NestedStack {
  rdsInstance: DatabaseInstance;

  constructor(
    scope: Construct,
    id: string,
    infrastuctureStack: WastageEcsInfrastureStack
  ) {
    super(scope, id);

    this.rdsInstance = new DatabaseInstance(
      this,
      "WastageEcsRdsDatabaseInstance",
      {
        vpc: infrastuctureStack.vpc,
        vpcSubnets: {
          subnetType: SubnetType.PRIVATE,
        },
        engine: DatabaseInstanceEngine.postgres({
          version: PostgresEngineVersion.VER_12_4,
        }),
        instanceType: new InstanceType("t2.micro"), // nano doesn't work?
        allocatedStorage: 5,
        maxAllocatedStorage: 10,
        // storageEncrypted: true, // "DB Instance class db.t2.micro does not support encryption at rest"
      }
    );
  }
}
class WastageEcsEphemeralStack extends NestedStack {
  constructor(
    scope: Construct,
    id: string,
    statefulStack: WastageEcsStatefulStack,
    infrastructureStack: WastageEcsInfrastureStack
  ) {
    super(scope, id);

    const image = ContainerImage.fromAsset("../../");

    const taskDefinition = new Ec2TaskDefinition(this, "WastageServerEc2Task", {
      volumes: [
        {
          name: "PersistedWastageServerVolume",
          host: {
            sourcePath: "/etc/persisted-wastage-server-storage",
          },
        },
      ],
    });

    const container = taskDefinition.addContainer(
      "WastageServerEc2TaskContainer",
      {
        image,
        memoryReservationMiB: 512,
        secrets: {
          DATABASE_USER: Secret.fromSecretsManager(
            statefulStack.rdsInstance.secret!,
            "username"
          ),
          DATABASE_PASSWORD: Secret.fromSecretsManager(
            statefulStack.rdsInstance.secret!,
            "password"
          ),
        },
        environment: {
          DATABASE_URL: `jdbc:postgresql://${statefulStack.rdsInstance.instanceEndpoint.socketAddress}/`,
        },
      }
    );
    container.addPortMappings({ containerPort: 8080, hostPort: 8080 });

    const service = new Ec2Service(this, "WastageServerEc2Service", {
      cluster: infrastructureStack.cluster,
      taskDefinition,
      desiredCount: 1,
    });

    statefulStack.rdsInstance.connections.allowFrom(service, Port.allTcp());

    infrastructureStack.loadBalancer.addRedirect(); // with no arguments it redirects HTTP to HTTPS

    const httpsListener = infrastructureStack.loadBalancer.addListener(
      "WastageServerHTTPSListerner",
      {
        protocol: ApplicationProtocol.HTTPS,
        certificates: [infrastructureStack.certificate],
        open: true,
      }
    );

    const httpsTargetGroup = httpsListener.addTargets(
      "Wastage Server HTTPS Target",
      {
        protocol: ApplicationProtocol.HTTP,
        targets: [
          service.loadBalancerTarget({
            containerName: container.containerName,
            containerPort: 8080,
          }),
        ],
      }
    );
  }
}

class WastageEcsRoutingStack extends NestedStack {
  constructor(
    scope: Construct,
    id: string,
    infrastructureStack: WastageEcsInfrastureStack
  ) {
    super(scope, id);

    new ARecord(this, "WastageARecord", {
      recordName: "wastage",
      zone: infrastructureStack.hostedZone,
      target: RecordTarget.fromAlias(
        new LoadBalancerTarget(infrastructureStack.loadBalancer)
      ),
      ttl: Duration.minutes(5),
    });
  }
}
