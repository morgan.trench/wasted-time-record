#!/usr/bin/env node
import { config } from "dotenv";
import "source-map-support/register";
import * as cdk from "@aws-cdk/core";
import { AwsEc2Stack } from "../lib/aws-ec2-stack";
import { WastageEcsStack } from "../lib/aws-ecs-stack";
config();

const app = new cdk.App();

const commonProps = {
  env: {
    region: process.env.CDK_DEFAULT_REGION,
    account: process.env.CDK_DEFAULT_ACCOUNT,
  },
};

const ec2Stack = new AwsEc2Stack(app, "WastageEc2Stack", commonProps);
const ecsStack = new WastageEcsStack(app, "WastageEcsStack", commonProps);
