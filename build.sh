#/bin/bash
cd server
./gradlew distTar
cd ..

rm -rf dist
mkdir dist
tar -xvf server/build/distributions/wasted-server-0.0.1.tar
mv wasted-server-0.0.1/* dist
rm wasted-server-0.0.1 -rf

mkdir dist/public

cd frontend
npm run build
cd ..
mv frontend/build/* dist/public

tar -zcvf dist.tar.gz dist