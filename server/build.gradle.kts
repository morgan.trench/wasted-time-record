import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val exposed_version: String by project
val h2_version: String by project
val kodein_version: String by project
val exposed_datetime_version: String by project

plugins {
    application
    kotlin("jvm") version "1.3.70"
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

group = "dev.morgantrench"
version = "0.0.1"

application {
    mainClassName = "io.ktor.server.netty.EngineMain"
}

repositories {
    mavenLocal()
    jcenter()
    maven { url = uri("https://kotlin.bintray.com/ktor") }
}

dependencies {
    implementation("org.jetbrains.kotlin", "kotlin-stdlib-jdk8", kotlin_version)
    implementation("io.ktor", "ktor-server-netty", ktor_version)
    implementation("ch.qos.logback", "logback-classic", logback_version)
    implementation("io.ktor", "ktor-server-core", ktor_version)
    implementation("io.ktor", "ktor-server-host-common", ktor_version)
    implementation("io.ktor", "ktor-locations", ktor_version)
    implementation("io.ktor", "ktor-metrics", ktor_version)
    implementation("io.ktor", "ktor-auth", ktor_version)
    implementation("io.ktor", "ktor-auth-jwt", ktor_version)
    implementation("io.ktor", "ktor-auth-ldap", ktor_version)
    implementation("io.ktor", "ktor-gson", ktor_version)
    implementation("io.ktor", "ktor-websockets", ktor_version)
    implementation("io.ktor", "ktor-network", ktor_version)
    implementation("io.ktor", "ktor-network-tls", ktor_version)
    implementation("io.ktor", "ktor-network-tls-certificates", ktor_version)

    testImplementation("io.ktor", "ktor-server-tests", ktor_version)

    implementation("io.ktor","ktor-client-apache", ktor_version)

    implementation("org.kodein.di", "kodein-di", kodein_version)
    implementation("org.kodein.di", "kodein-di-framework-ktor-server-jvm", kodein_version)
//    implementation("org.kodein.di", "kodein-di-framework-ktor-server-controller-jvm", kodein_version)

    implementation("com.h2database", "h2", h2_version)

    implementation("org.postgresql", "postgresql", "42.2.18")
//    compile("org.xerial:sqlite-jdbc:3.30.1")
    implementation("org.jetbrains.exposed", "exposed-core", exposed_version)
    implementation("org.jetbrains.exposed", "exposed-dao", exposed_version)
    implementation("org.jetbrains.exposed", "exposed-jdbc", exposed_version)
    implementation("org.jetbrains.exposed", "exposed-java-time", exposed_datetime_version)

}

kotlin.sourceSets["main"].kotlin.srcDirs("src")
kotlin.sourceSets["test"].kotlin.srcDirs("test")

sourceSets["main"].resources.srcDirs("resources")
sourceSets["test"].resources.srcDirs("testresources")
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    freeCompilerArgs = listOf("-Xinline-classes")
}