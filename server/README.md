# Deployment
#### HTTPS
As the point of this application is complaints, anonymity is a priority. To help achieve that, this server will force
the use of HTTPS if it has been configured.

To do so, add your variant of the following block to the application.conf file used by the server
``` HOCON
ktor {
    ...
    security {
        ssl {
            keyStore = build/key_store.jks
            keyAlias = self_signed
            keyStorePassword = changeit
            privateKeyPassword = changeit
        }
    }
    ...
}
```

The above block shows the default location the server looks for the certificate, to create a new one (which you should 
upon deployment) run the following command (modified as required):
``` bash
keytool -genkey -keyalg RSA -alias selfsigned -keystore keystore.jks -storepass password -validity 360 -keysize 2048
```
