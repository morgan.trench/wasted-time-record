package dev.morgantrench.persistance

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.`java-time`.timestamp
import java.time.Instant

sealed class WastageServerTable() : Table()

object Records : WastageServerTable() {
    val id: Column<Int> = integer("id").autoIncrement().uniqueIndex()
    val wastage: Column<Int> = integer("wastage")
    val complaint: Column<String> = text("complaint")
    val created: Column<Instant> = timestamp("created")
}

object Words : WastageServerTable() {
    val recordIndex: Column<Int> = reference("record_index", Records.id)
    val word: Column<String> = text("word")
}

object IgnoreWords : WastageServerTable() {
    val index: Column<Int> = integer("index").uniqueIndex()
        .autoIncrement() // Seems like have one the word column below breaks everything (table fails to be made? but no exception??) this is a temporary workaround
    val word: Column<String> = text("word")
}