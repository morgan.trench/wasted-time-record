package dev.morgantrench.persistance

import dev.morgantrench.MaintenanceExecutor
import dev.morgantrench.printerrln
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.config.*
import io.ktor.locations.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.kodein.di.DI
import org.kodein.di.instance
import java.io.File
import java.time.Instant
import java.util.concurrent.TimeUnit

lateinit var databaseUrl: String
lateinit var database: Database
var backupBody: (() -> Unit)? = null

@OptIn(KtorExperimentalAPI::class)
fun connectDatabase(di: DI): Database {
    // Extract url
    val config by di.instance<ApplicationConfig>()
    databaseUrl = config.property("ktor.deployment.databaseUrl").getString()

    val dbUser = config.propertyOrNull("ktor.security.databaseUser")?.getString()
    val dbPassword = config.propertyOrNull("ktor.security.databasePassword")?.getString()

    // Connect
    database = if (dbUser == null || dbPassword == null) {
        Database.connect(databaseUrl)
    } else {
        Database.connect(databaseUrl, user = dbUser, password = dbPassword)
    }

    // Create Tables if they don't already exist
    WastageServerTable::class.sealedSubclasses
        .mapNotNull { it.objectInstance }
        .forEach {
            println("Ensuring ${it.tableName} exists")
            transaction { SchemaUtils.create(it) }
        }

    // Backups
    backupBody = with(databaseUrl) {
        when {
            startsWith("jdbc:h2") -> {
                restoreH2FromFile(config)
                setupH2Backups(config)
            }
            else -> null
        }
    }
    return database
}

@KtorExperimentalLocationsAPI
@Location("/trigger-backup")
class TriggerBackup

fun Route.triggerBackup() {
    backupBody?.run {
        authenticate {
            get<TriggerBackup> {
                call.respond(invoke())
            }
        }
    }
}


private val compressionExtensions: Map<String, String> = mapOf("GZIP" to "gz", "ZIP" to "zip", "" to "sql")
private val extension2Compression: Map<String, String> = compressionExtensions.entries.map { it.value to it.key }.toMap()

@OptIn(KtorExperimentalAPI::class)
fun setupH2Backups(config: ApplicationConfig): () -> Unit {
    val backupDirectory = File(config.property("ktor.deployment.databaseBackupDirectory").getString()).apply { mkdirs() }
    val compressionMethod = config.property("ktor.deployment.databaseBackupCompression").getString()
    val compressionExtension = requireNotNull(compressionExtensions[compressionMethod]) {
        "Unsupported compression method, must be one of ${
            compressionExtensions.keys.joinToString(", ")
        }"
    }

    fun cleanupBackupDirectory() = (backupDirectory.listFiles()?.toList() ?: emptyList<File>())
        .sortedBy { it.lastModified() }
        .dropLast(30)
        .forEach { it.delete() }

    fun takeH2Backup(prefix: String? = null) {
        runBlocking {
            val backupFile = File(backupDirectory, "${prefix?.plus("-") ?: ""}${Instant.now().epochSecond}.$compressionExtension")
            transaction {
                connection.prepareStatement(
                    "SCRIPT TO '${backupFile.path}'".plus(if (compressionMethod.isEmpty()) "" else " COMPRESSION $compressionMethod"),
                    false
                ).executeQuery()
            }
            cleanupBackupDirectory()
        }
    }

    // Schedule regular backups
    val automaticBackupPrefix = "auto-backup"
    MaintenanceExecutor.scheduleAtFixedRate({ takeH2Backup(automaticBackupPrefix) }, 1, 1, TimeUnit.DAYS)

    val manualBackupPrefix = "manual-backup"
    return { takeH2Backup(manualBackupPrefix) }
}

@OptIn(KtorExperimentalAPI::class)
fun restoreH2FromFile(config: ApplicationConfig) {
    val backupDirectory = File(config.property("ktor.deployment.databaseBackupDirectory").getString()).apply { mkdirs() }
    val compressionMethod = config.property("ktor.deployment.databaseBackupCompression").getString()
    val databaseFile = if (!databaseUrl.contains("jdbc:h2:mem:")) File(databaseUrl.removePrefix("jdbc:h2:").plus(".mv.db")) else null
    val restoreFromBackup = !(databaseFile?.exists() ?: false)
    if (restoreFromBackup) {
        backupDirectory.listFiles()?.toList()?.maxBy { it.lastModified() }?.also { backupFile ->
            transaction {
                val compression = extension2Compression[backupFile.extension]
                if (compression != null) {
                    println("Restoring database from backup")
                    connection.prepareStatement(
                        "RUNSCRIPT FROM '${backupFile.path}'".plus(if (compressionMethod.isEmpty()) "" else " COMPRESSION $compression"),
                        false
                    ).executeUpdate()
                } else {
                    printerrln(
                        "Unable to restore from latest backup due to unsupported file format, should be one of ${
                            compressionExtensions.values.joinToString(
                                ", "
                            ) { ".$it" }
                        }"
                    )
                }
            }
        }
    }
}