package dev.morgantrench

import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import dev.morgantrench.persistance.connectDatabase
import dev.morgantrench.persistance.triggerBackup
import dev.morgantrench.record.RecordService
import dev.morgantrench.record.record
import dev.morgantrench.search.search
import dev.morgantrench.totals.TotalsService
import dev.morgantrench.totals.notifyTotals
import dev.morgantrench.totals.totals
import dev.morgantrench.words.WordsService
import dev.morgantrench.words.notifyWords
import dev.morgantrench.words.wordFilters
import dev.morgantrench.words.words
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.config.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import io.ktor.http.content.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.util.*
import io.ktor.websocket.*
import org.jetbrains.exposed.sql.Database
import org.kodein.di.bind
import org.kodein.di.eagerSingleton
import org.kodein.di.ktor.di
import org.kodein.di.singleton
import org.slf4j.event.Level
import java.io.File
import java.security.SecureRandom
import java.time.Duration
import java.time.Instant
import java.util.*

fun main(args: Array<String>) = io.ktor.server.netty.EngineMain.main(args)

@OptIn(KtorExperimentalAPI::class)
@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    install(Locations)

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        method(HttpMethod.Post)
        method(HttpMethod.Put)
        method(HttpMethod.Get)
        header(HttpHeaders.Authorization)
        header(HttpHeaders.ContentType)
        allowCredentials = true
        anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
    }

    // Sharing a gson configuration/instance between ContentNegotiation and Kodein, TODO try and tidy, make val/const
    var gson: Gson? = null
    install(ContentNegotiation) {
        gson {
            setPrettyPrinting()
            serializeNulls()
            registerTypeAdapter(Instant::class.java, object : TypeAdapter<Instant>() {
                override fun write(jw: JsonWriter, p1: Instant?) {
                    if (p1 != null) jw.value(p1.toEpochMilli()) else jw.nullValue()
                }

                override fun read(jr: JsonReader): Instant = Instant.ofEpochMilli(jr.nextLong())
            })
            gson = create()
        }
    }

    install(DataConversion)

    install(DefaultHeaders) {
        header("X-Engine", "Ktor") // will send this header with each response
    }

    install(Authentication) {
        basic {
            validate { credentials ->
                if (credentials.name == "admin" && credentials.password == "admin") UserIdPrincipal(credentials.name) else null
            }
        }
    }

    install(WebSockets) {
        pingPeriod = Duration.ofSeconds(30)
    }

    val config = environment.config
    // Force redirect to https if available
    if (config.propertyOrNull("ktor.security.ssl") != null){
        install(HttpsRedirect){
            config.propertyOrNull("ktor.deployment.sslPort")?.also { sslPort = it.getString().toInt() }
        }
    }

    di {
        bind<ApplicationConfig>() with singleton { config }
        bind<ApplicationEvents>() with singleton { environment.monitor }
        bind<Database>() with eagerSingleton { connectDatabase(di) }
        bind<Gson>() with singleton { gson!! }
        bind<RecordService>() with eagerSingleton { RecordService(di) }
        bind<WordsService>() with eagerSingleton { WordsService(di) }
        bind<TotalsService>() with eagerSingleton { TotalsService(di) }
    }

    routing {

        // API
        route("api") {
            record()
            words()
            search()
            totals()
            wordFilters()
            triggerBackup()
        }

        // WebSockets
        route("ws") {
            notifyWords()
            notifyTotals()
        }

        // Serve Frontend
        static {
            File("public/index.html").run {
                parentFile.run {
                    mkdirs()
                    files(name)
                }
                if (!exists()) File(javaClass.getResource("/${path.replace("\\", "/")}").file).copyTo(this)
                default(path)

                // For each 'route' of the served SPA, serve in index html // TODO mabye find a less gross way of doing this, is this even gross?
                config.propertyOrNull("ktor.deployment.spaRoutes")?.getString()?.split(',')?.forEach { file(it, path) }
            }
        }
    }
}
