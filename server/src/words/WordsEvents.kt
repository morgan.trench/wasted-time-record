package dev.morgantrench.words

import io.ktor.application.*
import io.ktor.util.*

@KtorExperimentalAPI
val WordsRecorded = EventDefinition<List<String>>()
val WordFiltersAdded = EventDefinition<List<String>>()
val WordFiltersRemoved = EventDefinition<List<String>>()