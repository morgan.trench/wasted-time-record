package dev.morgantrench.words

import dev.morgantrench.persistance.IgnoreWords
import dev.morgantrench.persistance.Words
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

private fun ResultRow.toWordUsage() = WordUsage(this[Words.word], this[Words.word.count()])

private fun Iterable<ResultRow>.toWordUsages() = this.map { it.toWordUsage() }

fun findWordUsage(word: String): WordUsage? =
    transaction {
        Words.slice(listOf(Words.word, Words.word.count())).select { Words.word eq word }.groupBy(Words.word)
            .singleOrNull()?.toWordUsage()
    }

fun allWordUsages(offset: Long? = null, size: Int? = null) = transaction {
    Words.slice(listOf(Words.word, Words.word.count()))
        .select { Words.word notInSubQuery IgnoreWords.slice(IgnoreWords.word).selectAll() }.groupBy(Words.word)
        .orderBy(Words.word.count(), SortOrder.DESC).toWordUsages()
}

fun lookupRecordsIdsForWords(vararg words: String): List<Int> = transaction {
    Words.select { Words.word inList words.toList() }.map { it[Words.recordIndex] }.distinct()
}

fun storeWords(recordIndex: Int, words: List<String>) =
    transaction {
        Words.batchInsert(words) { uw ->
            this[Words.word] = uw
            this[Words.recordIndex] = recordIndex
        }
    }.map { it[Words.word] }

fun getAllIgnoreWords() =
    transaction { IgnoreWords.slice(IgnoreWords.word).selectAll().map { it[IgnoreWords.word] } }

fun addToIgnoreWords(vararg words: String) = transaction {
    val wordsToAdd: Set<String> = words.toSet().let {
        it.subtract(IgnoreWords.slice(IgnoreWords.word).select { IgnoreWords.word inList words.toList() }
            .map { it[IgnoreWords.word] }.toSet())
    }
    IgnoreWords.batchInsert(wordsToAdd) { toAdd ->
        this[IgnoreWords.word] = toAdd
    }.map { it[IgnoreWords.word] }
}

fun removeFromIgnoreWords(vararg words: String) = transaction {
    val toDelete = IgnoreWords.select { IgnoreWords.word inList words.toList() }.map { it[IgnoreWords.word] }
    IgnoreWords.deleteWhere { IgnoreWords.word inList words.toList() }
    toDelete
}