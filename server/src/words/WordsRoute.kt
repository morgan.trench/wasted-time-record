package dev.morgantrench.words

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.kodein.di.instance
import org.kodein.di.ktor.di


@OptIn(KtorExperimentalLocationsAPI::class)
@Location("/words")
class WordsRoute {

    @Location("/{word}")
    class GetOne(val word: String, val wordsRoute: WordsRoute)

    @Location("")
    class GetAll(val size: Int = Int.MAX_VALUE, val offset: Long = 0, val wordsRoute: WordsRoute)
}

@OptIn(KtorExperimentalLocationsAPI::class)
fun Route.words() {
    val wordService by di().instance<WordsService>()

    get<WordsRoute.GetAll> { getAll ->
        call.respond(wordService.getAllWordUsages(getAll.offset, getAll.size).asMap())
    }

    get<WordsRoute.GetOne> { route ->
        wordService.getWordUsage(route.word)?.apply { call.respond(count) } ?: call.respond(HttpStatusCode.NotFound)
    }
}

@OptIn(KtorExperimentalLocationsAPI::class)
@Location("/words2ignore")
class Words2IgnoreRoute {

    @Location("")
    class GetAll(val words2IgnoreRoute: Words2IgnoreRoute)

    @Location("/add")
    class Add(val words2IgnoreRoute: Words2IgnoreRoute)

    @Location("/remove")
    class Remove(val words2IgnoreRoute: Words2IgnoreRoute)
}


@OptIn(KtorExperimentalLocationsAPI::class)
fun Route.wordFilters() {
    val wordService by di().instance<WordsService>()
    authenticate {
        get<Words2IgnoreRoute.GetAll> { call.respond(wordService.getIgnoredWords()) }

        post<Words2IgnoreRoute.Add> {
            wordService.addIgnoredWords(*call.receive<List<String>>().toTypedArray())
            call.respond(HttpStatusCode.OK)
        }

        post<Words2IgnoreRoute.Remove> {
            wordService.removeIgnoredWords(*call.receive<List<String>>().toTypedArray())
            call.respond(HttpStatusCode.OK)
        }
    }
}

fun Iterable<WordUsage>.asMap() = fold(mutableMapOf<String, Long>()) { acc, wordUsage ->
    acc[wordUsage.word] = wordUsage.count
    acc
}

