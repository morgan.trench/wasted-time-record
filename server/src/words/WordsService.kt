package dev.morgantrench.words

import dev.morgantrench.record.Record
import dev.morgantrench.record.RecordCreated
import io.ktor.application.*
import io.ktor.util.*
import org.kodein.di.DI
import org.kodein.di.instance

@OptIn(KtorExperimentalAPI::class)
class WordsService(di: DI) {

    // Words
    private val applicationEvents by di.instance<ApplicationEvents>()

    fun getWordUsage(word: String): WordUsage? = findWordUsage(word)

    fun getAllWordUsages(offset: Long, size: Int): Iterable<WordUsage> = allWordUsages(offset, size)

    fun getRecordsIdsForWords(vararg words: String) = lookupRecordsIdsForWords(*words)

    private fun processRecord(record: Record) {
        record.storeWords().also { storedWords -> applicationEvents.raise(WordsRecorded, storedWords) }
    }

    // Word Filtering
    fun getIgnoredWords() = getAllIgnoreWords()

    fun addIgnoredWords(vararg words: String) =
        addToIgnoreWords(*words).also { wordsToIgnore -> applicationEvents.raise(WordFiltersAdded, wordsToIgnore) }

    fun removeIgnoredWords(vararg words: String) =
        removeFromIgnoreWords(*words).also { wordsToUnIgnore -> applicationEvents.raise(WordFiltersRemoved, wordsToUnIgnore) }

    init {
        applicationEvents.subscribe(RecordCreated, ::processRecord)
    }
}

private val nonAlphaNumberics = Regex("[^A-Za-z0-9]")
val Record.processedWords: List<String>
    get() = this.complaint
        .split("\\s".toRegex())
        .map { it.replace(nonAlphaNumberics, "").toLowerCase() }
        .filter { it.isNotEmpty() && it.isNotBlank() }

fun Record.storeWords() = storeWords(id, processedWords)

