package dev.morgantrench.words

import com.google.gson.Gson
import io.ktor.application.*
import io.ktor.http.cio.websocket.*
import io.ktor.routing.*
import io.ktor.util.*
import io.ktor.websocket.*
import kotlinx.coroutines.runBlocking
import org.kodein.di.instance
import org.kodein.di.ktor.di
import java.util.*

@OptIn(KtorExperimentalAPI::class)
fun Route.notifyWords() {
    val sessions: MutableSet<DefaultWebSocketSession> =
        Collections.synchronizedSet(mutableSetOf<DefaultWebSocketSession>())
    webSocket("words") { // TODO find a way to have the @Location feature/annotation work with websockets if it makes sense at some point, not really any parameters to map
        try {
            sessions += this
            while (true) {
                incoming.receive()
            }
        } finally {
            sessions -= this
        }
    }
    val gson by di().instance<Gson>()
    fun WordMsg.toJson(): String = gson.toJson(this)
    fun String.sendToAll() = runBlocking {
        val msg = Frame.Text(this@sendToAll)
        sessions.forEach {
            it.outgoing.send(msg)
        }
    }

    fun notifyWordsRecorded(words: List<String>) {
        val words2Ignore = getAllIgnoreWords()
        words.filter { word -> word !in words2Ignore }.asRecordedEvent().toJson().sendToAll()
    }
    fun notifyWordsFiltered(words: List<String>) = words.asFilterAddedEvent().toJson().sendToAll()
    fun notifyWordsUnFiltered(words: List<String>) = words.asFilterRemovedEvent().toJson().sendToAll()

    val applicationEvents by di().instance<ApplicationEvents>()
    applicationEvents.subscribe(WordsRecorded, ::notifyWordsRecorded)
    applicationEvents.subscribe(WordFiltersAdded, ::notifyWordsFiltered)
    applicationEvents.subscribe(WordFiltersRemoved, ::notifyWordsUnFiltered)

}