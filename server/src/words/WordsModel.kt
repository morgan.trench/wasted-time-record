package dev.morgantrench.words

data class WordUsage(val word: String, val count: Long)

enum class WordEventType{
    Recorded,
    FilterAdded,
    FilterRemoved
}
class WordMsg(val type: WordEventType, val words: List<String>)

fun List<String>.asRecordedEvent() = WordMsg(WordEventType.Recorded, this)
fun List<String>.asFilterAddedEvent() = WordMsg(WordEventType.FilterAdded, this)
fun List<String>.asFilterRemovedEvent() = WordMsg(WordEventType.FilterRemoved, this)