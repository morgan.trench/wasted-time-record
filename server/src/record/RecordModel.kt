package dev.morgantrench.record

import java.time.Instant

data class IncomingRecord(
    val wastage: Int,
    val complaint: String,
    val created: Long? = null
)

data class Record(
    val id: Int,
    val wastage: Int,
    val complaint: String,
    val created: Instant
)