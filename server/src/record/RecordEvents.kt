package dev.morgantrench.record

import io.ktor.application.*
import io.ktor.util.*

@KtorExperimentalAPI
val RecordCreated = EventDefinition<Record>()