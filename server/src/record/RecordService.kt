package dev.morgantrench.record

import io.ktor.application.*
import io.ktor.util.*
import org.kodein.di.DI
import org.kodein.di.instance

@OptIn(KtorExperimentalAPI::class)
class RecordService(di: DI) {

    private val applicationEvents by di.instance<ApplicationEvents>()

    fun getRecord(id: Int) = findRecords(id).singleOrNull()
    fun getRecords(vararg ids: Int) = findRecords(*ids)
    fun getAllRecords(size: Int, offset: Long) = allRecords(size, offset)

    fun storeRecord(record: IncomingRecord) = record
        .save()
        .also { savedRecord -> applicationEvents.raise(RecordCreated, savedRecord) }

}

