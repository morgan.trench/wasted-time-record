package dev.morgantrench.record

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.kodein.di.instance
import org.kodein.di.ktor.di
import java.time.Instant

@OptIn(KtorExperimentalLocationsAPI::class)
@Location("/record")
class RecordRoute {

    @Location("")
    class CreateRecord(val recordRoute: RecordRoute)

    @Location("/{id}")
    data class GetRecord(val id: Int, val recordRoute: RecordRoute)

    @Location("")
    data class GetRecords(val size: Int = Int.MAX_VALUE, val offset: Long = 0, val recordRoute: RecordRoute)
}

@OptIn(KtorExperimentalLocationsAPI::class)
fun Route.record() {
    val recordService by di().instance<RecordService>()

    post<RecordRoute.CreateRecord> {
        call.respond(recordService.storeRecord(call.receive<IncomingRecord>().let{ it.copy(created = it.created ?: Instant.now().toEpochMilli()) }))
    }

    get<RecordRoute.GetRecord> { route ->
        recordService.getRecord(route.id)?.also { call.respond(it) } ?: call.respond(HttpStatusCode.NotFound)
    }

    get<RecordRoute.GetRecords> { route ->
        call.respond(recordService.getAllRecords(route.size, route.offset))
    }
}