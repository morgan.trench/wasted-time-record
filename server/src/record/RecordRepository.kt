package dev.morgantrench.record

import dev.morgantrench.persistance.Records
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Instant

private fun ResultRow.toRecord() =
    Record(this[Records.id], this[Records.wastage], this[Records.complaint], this[Records.created])

private fun Iterable<ResultRow>.toRecords() = map { it.toRecord() }

fun findRecords(vararg ids: Int): Iterable<Record> =
    transaction { (ids.singleOrNull()?.let { Records.select { Records.id eq ids.single() } } ?: Records.select { Records.id inList ids.toList() }).toRecords() }

fun allRecords(size: Int, offset: Long): Iterable<Record> = transaction { Records.selectAll().limit(size, offset).toRecords() }

fun IncomingRecord.save() = transaction {
    Records.insert {
        it[wastage] = this@save.wastage
        it[complaint] = this@save.complaint
        it[created] = Instant.ofEpochMilli(this@save.created ?: Instant.now().toEpochMilli())
    }.resultedValues!!.single().toRecord()
}
