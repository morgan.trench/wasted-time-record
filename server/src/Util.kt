package dev.morgantrench

import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService

private val executor: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()

object MaintenanceExecutor : ScheduledExecutorService by executor

fun printerrln(message: Any?) = System.err.println(message)