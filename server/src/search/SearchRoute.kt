package dev.morgantrench.search

import dev.morgantrench.record.RecordService
import dev.morgantrench.words.WordsService
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.kodein.di.instance
import org.kodein.di.ktor.di

@OptIn(KtorExperimentalLocationsAPI::class)
@Location("/search")
class RecordRoute {

    @Location("")
    class Search(val recordRoute: RecordRoute)
}

data class SearchBody(val returnRecords: Boolean = false, val containing: List<String>?)

@OptIn(KtorExperimentalLocationsAPI::class)
fun Route.search() {
    val recordService by di().instance<RecordService>()
    val wordService by di().instance<WordsService>()

    post<RecordRoute.Search> {
        val criteria = call.receive<SearchBody>()
        if (criteria.containing.isNullOrEmpty()) {
            call.respond(HttpStatusCode.BadRequest)
            return@post
        }
        val ids = wordService.getRecordsIdsForWords(*criteria.containing.toTypedArray())
        call.respond(if (criteria.returnRecords) recordService.getRecords(*ids.toIntArray()) else ids)
    }
}