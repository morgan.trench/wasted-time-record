package dev.morgantrench.totals

import dev.morgantrench.persistance.Records
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Instant

fun totalForPeriod(period : Pair<Instant, Instant>) = totalsForPeriods(period).single()

fun totalsForPeriods(vararg periods : Pair<Instant, Instant>) =
    transaction {
        periods.map { period ->
            period to Records.select { (Records.created greaterEq period.first) and (Records.created lessEq period.second) }.sumBy { it[Records.wastage] }
        }
    }