package dev.morgantrench.totals

import com.google.gson.Gson
import dev.morgantrench.record.Record
import dev.morgantrench.record.RecordCreated
import io.ktor.application.*
import io.ktor.http.cio.websocket.*
import io.ktor.routing.*
import io.ktor.util.*
import io.ktor.websocket.*
import kotlinx.coroutines.runBlocking
import org.kodein.di.instance
import org.kodein.di.ktor.di
import java.util.*

@OptIn(KtorExperimentalAPI::class)
fun Route.notifyTotals() {
    val sessions: MutableSet<DefaultWebSocketSession> =
        Collections.synchronizedSet(mutableSetOf<DefaultWebSocketSession>())
    webSocket("totals") { // TODO find a way to have the @Location feature/annotation work with websockets if it makes sense at some point, not really any parameters to map
        try {
            sessions += this
            while (true) {
                incoming.receive()
            }
        } finally {
            sessions -= this
        }
    }
    val gson by di().instance<Gson>()
    fun UpdateTotalsMsg.sendToAll() = runBlocking {
        val msg = Frame.Text(gson.toJson(this@sendToAll))
        sessions.forEach {
            it.outgoing.send(msg)
        }
    }

    fun notifyTotalsUpdated(record: Record) {
        record.asUpdateTotalsMsg().sendToAll()
    }

    val applicationEvents by di().instance<ApplicationEvents>()
    applicationEvents.subscribe(RecordCreated, ::notifyTotalsUpdated)

}