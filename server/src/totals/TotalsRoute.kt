package dev.morgantrench.totals

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.kodein.di.instance
import org.kodein.di.ktor.di
import java.time.Instant

@OptIn(KtorExperimentalLocationsAPI::class)
@Location("/totals")
class TimeRoute {
    @Location("")
    class Totals(val timeRoute: TimeRoute)
}

private infix fun Instant.isBefore(time: Instant) = this.isBefore(time)
private fun Pair<Instant, Instant>.ensureOrder() = if (first isBefore second) this else second to first

@OptIn(KtorExperimentalLocationsAPI::class)
fun Route.totals() {
    val totalsService by di().instance<TotalsService>()

    post<TimeRoute.Totals> {
        val periods =
            call.receive<TotalRequestBody>().boundaries.map { Instant.ofEpochMilli(it.from) to Instant.ofEpochMilli(it.to) }
                .map(Pair<Instant, Instant>::ensureOrder)
        call.respond(totalsService.getTotalsForPeriods(*periods.toTypedArray()).map(::TotalsResponseBody))
    }
}