package dev.morgantrench.totals

import dev.morgantrench.record.Record
import java.time.Instant

data class Range(val from: Long, val to: Long)
inline class TotalRequestBody(val boundaries: List<Range>)

data class TotalsResponseBody(val from: Instant, val to: Instant, val total: Int) {
    constructor(data: Pair<Pair<Instant, Instant>, Int>) : this(data.first.first, data.first.second, data.second)
}

data class UpdateTotalsMsg(val duration: Int, val timestamp: Instant) {
    constructor(record: Record) : this(record.wastage, record.created)
}

fun Record.asUpdateTotalsMsg() = UpdateTotalsMsg(this)