package dev.morgantrench.totals

import io.ktor.util.*
import org.kodein.di.DI
import java.time.Instant

@OptIn(KtorExperimentalAPI::class)
class TotalsService(di: DI) {
    fun getTotalForPeriod(period: Pair<Instant, Instant>) = totalForPeriod(period)
    fun getTotalsForPeriods(vararg periods: Pair<Instant, Instant>) = totalsForPeriods(*periods)
}

